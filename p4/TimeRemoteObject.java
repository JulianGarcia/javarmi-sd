import java.rmi.*;
import java.rmi.server.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeRemoteObject extends UnicastRemoteObject implements TimeRemote {
  TimeRemoteObject() throws RemoteException {
    super();
  }

  public long showTime() {

    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    long dateInMiliseconds = calendar.getTime().getTime();
    //System.out.println("soy timeRemoteObject y mi hora es: " + sdf.format(dateInMiliseconds));
    return dateInMiliseconds;

  }
}