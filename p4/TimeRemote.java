import java.rmi.*;

public interface TimeRemote extends Remote {
  public long showTime() throws RemoteException;
}