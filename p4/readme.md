# Java project that implements the Cristian clock sincronization w/ Java RMI

## Author: 
 Julian I. Garcia - UNPSJB Argentina

## To compile:
 - <code>javac *.java</code>

## To execute need at least 3 terminals:
 - 1°: execute <code>rmiregistry 1099</code>. You can replace 1099 for any port you like.
 - 2°: execute <code>java Server</code>
 - 3°: execute <code>java Client</code>