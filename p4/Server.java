import java.rmi.Naming;                    /* lookup         */
import java.rmi.registry.Registry;   

public class Server {
  public static void main(String args[]) {
    try {
      System.setProperty(
				"java.rmi.server.codebase",
				"file:/home/julian/sistemas-distribuidos/Práctica/practica2/p4/");
      TimeRemote timeRemoteObject = new TimeRemoteObject();
      String rname = "//localhost:" + Registry.REGISTRY_PORT  + "/TimeRemoteObject";
      Naming.rebind(rname, timeRemoteObject);
      //System.out.println(timeRemoteObject.showTime());
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}