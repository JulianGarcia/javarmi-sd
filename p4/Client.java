import java.rmi.Naming; /* lookup         */
import java.rmi.registry.Registry;
import java.text.SimpleDateFormat;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import java.net.MalformedURLException; /* Exceptions...  */
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Client {

  public static long localTimer;
  public static long NewTime;
  public static long rtt;
  public static long deriva;
  public static SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss");
  public static Window window;

  public static class InternalClock extends Thread {

    private long tita;
    private long auxTimer;

    public InternalClock(long difference) {
      tita = difference;
      localTimer = System.currentTimeMillis() + tita;
      //System.out.println("Internal clock localTimer: " + SDF.format(localTimer));
    }

    public void run() {
      while (true) {
        try {
          Thread.sleep(1000);
          deriva = localTimer - NewTime;
          if(deriva<0) {
            tita = 1500;
          } else {
            if (deriva>0) {
              tita = 500;
            } else {
              tita = 1000;
            }
          }
          localTimer += tita;
          //System.out.println("LocalTimer: " + SDF.format(localTimer) + " - Tita: " + tita);
          window.clientTimeField.setText(SDF.format(localTimer));
          window.clientTitaField.setText(Long.toString(tita));
        } catch (Exception e) {
          System.out.println("internal clock: " + e);
        }
      }
    }
  }

  public static class Update {
    private long currentTime1;
    private long currentTime4;
    private long auxRtt;
    
    public void runUpdate() {
      try {
        NewTime = 0;
        auxRtt = 0;
        rtt = 0;
        currentTime1 = System.currentTimeMillis();
        int i = 10; // pide la hora 10 veces
        String rname = "//localhost:" + Registry.REGISTRY_PORT + "/TimeRemoteObject";
        TimeRemote timeRemoteObject;
        for (int j = 0; j < i; j++) {
          timeRemoteObject = (TimeRemote) Naming.lookup(rname);
          NewTime += timeRemoteObject.showTime(); //acumulo las horas que me llegan del servidor
          currentTime4 = System.currentTimeMillis();
          auxRtt = auxRtt + (currentTime4 - currentTime1)/2; //acumulo rtt por cada vez que pido la hs al servidor
          currentTime1 = System.currentTimeMillis();
        }
        NewTime /= i;
        rtt = auxRtt/i;
        window.serverTimeField.setText(SDF.format(NewTime));
        //System.out.println("New Time: " + SDF.format(NewTime) + " - rtt: " + rtt);
        NewTime = NewTime + rtt;
      } catch (MalformedURLException e) {
        e.printStackTrace();
      } catch (RemoteException e) {
        e.printStackTrace();
      } catch (NotBoundException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Window
   */
  public static class Window {
    private JFrame myFrame;
    // Added by me
    private JPanel contentPane;
    private JPanel myPanel;
    private JLabel clientTime=null;
    private JLabel clientTita=null;
    private JLabel clientTimeField=null;
    private JLabel clientTitaField=null;
    private JLabel serverTime=null;
    private JLabel serverTimeField=null;
    private Color myColor=new Color(200, 102, 204);
    private Font myFont11=new Font("Tahoma", 1, 11);
    private Font myFont12bold=new Font("Tahoma", Font.BOLD, 12);
    private Font myFont11bold=new Font("Tahoma", Font.BOLD, 11);
   
    
    public void createComponents() {
      contentPane = new JPanel();
      contentPane.setOpaque(true);
      contentPane.setBackground(Color.WHITE);
      contentPane.setLayout(new GridBagLayout());
      contentPane.setBorder(BorderFactory.createTitledBorder("My Program"));

      clientTime=new JLabel("Hora Cliente: ");
      clientTime.setLabelFor(clientTime);
      clientTime.setFont(myFont11);
      clientTime.setForeground(Color.white);
      clientTita=new JLabel("Deriva: ");
      clientTita.setLabelFor(clientTita);
      clientTita.setFont(myFont11);
      clientTita.setForeground(Color.white);
      clientTimeField=new JLabel();
      clientTimeField.setLabelFor(clientTimeField);
      clientTimeField.setFont(myFont11bold);
      clientTimeField.setForeground(Color.white);
      clientTitaField=new JLabel();
      clientTitaField.setLabelFor(clientTitaField);
      clientTitaField.setFont(myFont11bold);
      clientTitaField.setForeground(Color.white);

      serverTime=new JLabel("Hora servidor: ");
      serverTime.setLabelFor(serverTime);
      serverTime.setFont(myFont11);
      serverTime.setForeground(Color.white);
      serverTimeField=new JLabel();
      serverTimeField.setLabelFor(serverTimeField);
      serverTimeField.setFont(myFont11bold);
      serverTimeField.setForeground(Color.white);
      

      //Panel
      myPanel=new JPanel();
      myPanel.setOpaque(true);
      myPanel.setBorder(BorderFactory.createTitledBorder("Cristian test"));
      myPanel.setBackground(myColor);
      myPanel.setLayout(new GridLayout(3, 3, 3, 3));
      myPanel.add(clientTime);
      myPanel.add(clientTimeField);
      myPanel.add(clientTita);
      myPanel.add(clientTitaField);
      myPanel.add(serverTime);
      myPanel.add(serverTimeField);
      //----------------------------------------------------------
      contentPane.add(myPanel);

      myFrame=new JFrame();
      myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      //myFrame.setPreferredSize(new Dimension(400,300));//width:400px, height:300px
      myFrame.setLocationRelativeTo(null);//to show at center of screen
      myFrame.setTitle("Ejercicio 4 - Laboratorio 2 - Sistemas Distribuidos");
      //myFrame.add(myPanel);
      myFrame.setContentPane(contentPane);
      myFrame.pack();//this alone will not give the frame a size
      myFrame.setVisible(true);
  }   
  }

  public static void main(String args[]) {
    try {
      //long syncTimePeriod = Integer.parseInt(args[0]);
      //long variacion = Integer.parseInt(args[1]);
      window = new Window();
      window.createComponents();
      Client.Update update = new Update();
      Client.InternalClock ic = new InternalClock(30000);
      ic.start();
      while (true) {
        Thread.sleep(1000);
        update.runUpdate();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}