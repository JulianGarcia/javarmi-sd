/*
 * Servidor.java
 *
 * Contiene el código para instanciar y publicar el objetoRemoto en la rmiregistry
 */

import java.rmi.Naming; /* lookup         */
import java.rmi.registry.Registry; /* REGISTRY_PORT  */

/**
 * Servidor para el ejemplo de RMI. Exporte un metodo para sumar dos enteros y
 * devuelve el resultado.
 */
public class Servidor {

    /** Crea nueva instancia de Servidor rmi */
    public Servidor() {
        try {
            // Se indica a rmiregistry donde están las clases.
            // Cambiar el paht al sitio en el que esté. Es importante
            // mantener la barra al final.
            System.setProperty("java.rmi.server.codebase",
                    "file:/home/julian/sistemas-distribuidos/Práctica/practica2/p1/");
            // Se publica el objeto remoto
            InterfaceRemotaSR objetoRemotoSR = new ObjetoRemotoSR();
            String rnameSR = "//localhost:" + Registry.REGISTRY_PORT + "/ObjetoRemotoSR";
            System.out.println(Registry.REGISTRY_PORT);
            Naming.rebind(rnameSR, objetoRemotoSR);

            InterfaceRemotaPD objetoRemotoPD = new ObjetoRemotoPD();
            String rnamePD = "//localhost:" + Registry.REGISTRY_PORT + "/ObjetoRemotoPD";
            System.out.println(Registry.REGISTRY_PORT);
            Naming.rebind(rnamePD, objetoRemotoPD);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Servidor();
    }
}
