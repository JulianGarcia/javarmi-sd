/*
 * InterfaceRemotaPD.java
 *
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface remota con los métodos producto y division que se pueden llamar en remoto
 */
public interface InterfaceRemotaPD extends Remote{
  public int division (int a, int b) throws RemoteException;
  public int producto (int a, int b) throws RemoteException;    
}
