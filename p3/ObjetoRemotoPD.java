/*
 * ObjetoRemoto.java
 */

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Objeto que implementa la interfaz remota
 */
public class ObjetoRemotoPD extends UnicastRemoteObject 
    implements InterfaceRemotaPD
{
    /**
     * Construye una instancia de ObjetoRemoto
     * @throws RemoteException
     */
    protected ObjetoRemotoPD () throws RemoteException
    {
        super();
    }

    public int division(int a, int b)
    {
        System.out.println ("Dividiendo " + a + " / " + b +"...");
        return a/b;
    }

    public int producto(int a, int b)
    {
        System.out.println ("Multiplicando " + a + " * " + b +"...");
        return a*b;
    }
    
}
