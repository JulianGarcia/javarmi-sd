/*
 * Cliente.java
 *
 * Ejemplo de muy simple de rmi
 */

import java.rmi.Naming; /* lookup         */
import java.rmi.registry.Registry; /* REGISTRY_PORT  */

import java.net.MalformedURLException; /* Exceptions...  */
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMISocketFactory;

class TimeoutFactory extends RMISocketFactory {
    private int timeout;
 
    public TimeoutFactory(int timeout) {
        this.timeout = timeout;
    }

    public Socket createSocket(String host, int port) throws IOException {
    Socket ret = getDefaultSocketFactory().createSocket(host, port);
        ret.setSoTimeout(timeout * 1000);
        return ret;
    }
    public ServerSocket createServerSocket(int port) throws IOException {
        return getDefaultSocketFactory().createServerSocket(port);
    }
}

/*
 * Ejemplo de cliente rmi 
*/
public class Cliente{

    /** Crea nueva instancia de Cliente */
    public Cliente(String[] args, int operando1, int operando2) {
        try {
            RMISocketFactory.setSocketFactory(new TimeoutFactory(5));

            String alfa = args[0];
            String rnameSR = "//" + alfa + ":" + Registry.REGISTRY_PORT + "/ObjetoRemotoSR";
            InterfaceRemotaSR objetoRemotoSR = (InterfaceRemotaSR) Naming.lookup(rnameSR);

            String rnamePD = "//" + alfa + ":" + Registry.REGISTRY_PORT + "/ObjetoRemotoPD";
            InterfaceRemotaPD objetoRemotoPD = (InterfaceRemotaPD) Naming.lookup(rnamePD);

            String operacion = args[1];

            System.out.println(Registry.REGISTRY_PORT);
            switch (operacion) {
                case "suma":
                    int s = objetoRemotoSR.suma(operando1, operando2);
                    System.out.println(args[2] + " + " + args[3] + " = " + s + "\n");
                    break;
                case "resta":
                    int r = objetoRemotoSR.resta(operando1, operando2);
                    System.out.println(args[2] + " - " + args[3] + " = " + r + "\n");
                    break;
                case "producto":
                    int p = objetoRemotoPD.producto(operando1, operando2);
                    System.out.println(args[2] + " * " + args[3] + " = " + p + "\n");
                    break;
                case "division":
                    int d = objetoRemotoPD.division(operando1, operando2);
                    System.out.println(args[2] + " / " + args[3] + " = " + d + "\n");
                    break;
                default:
                    break;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Cliente(args, Integer.parseInt(args[2]), Integer.parseInt(args[3]));
    }

}
