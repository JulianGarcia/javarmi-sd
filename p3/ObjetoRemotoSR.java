/*
 * ObjetoRemoto.java
 */

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.TimeUnit;

/**
 * Objeto que implementa la interfaz remota
 */
public class ObjetoRemotoSR extends UnicastRemoteObject 
    implements InterfaceRemotaSR
{
    /**
     * Construye una instancia de ObjetoRemoto
     * @throws RemoteException
     */
    protected ObjetoRemotoSR () throws RemoteException
    {
        super();
    }

    /**
     * Obtiene la suma de los sumandos que le pasan y la devuelve.
     */
    public int suma(int a, int b) 
    {
	    try {
            System.out.println ("Sumando " + a + " + " + b +"...");
            TimeUnit.SECONDS.sleep(6);
        } catch (InterruptedException e) {
            System.err.println("interrupt exception in OR suma");
        }
        return a+b;
    }

    public int resta(int a, int b)
    {
        System.out.println ("Restando " + a + " - " + b +"...");
        return a-b;
    }    
}
